This is a simple demo of animations using canvas and socket.io libs. 
Installation:

```bash
npm install
```
If you don't have http-server, just install it


```bash
npm install -g http-server
```

Then run
```bash
node server/server.js
```
and
```bash
http-server
```
Open http://0.0.0.0:8080 in two different browsers.
Use arrow keys to move your square, hold shift for turbo move, space for jump and
z for shot. Red square represents your "opponent".



In this game sprite from the page  [gameart2d.com](https://www.gameart2d.com/cute-girl-free-sprites.html) has been used.